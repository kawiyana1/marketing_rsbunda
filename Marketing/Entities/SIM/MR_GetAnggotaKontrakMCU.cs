//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Marketing.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class MR_GetAnggotaKontrakMCU
    {
        public short Customer_ID { get; set; }
        public string IDJasa { get; set; }
        public string NoAnggota { get; set; }
        public string Nama { get; set; }
        public bool Active { get; set; }
        public string Klp { get; set; }
        public string JabatanPerusahaan { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string ALamat { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string Gender { get; set; }
        public string NamaDepartemen { get; set; }
    }
}
