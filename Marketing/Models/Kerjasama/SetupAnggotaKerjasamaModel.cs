﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marketing.Models.Kerjasama
{
    public class SetupAnggotaKerjasamaModel
    {
        public decimal IdPerusahaan { get; set; }
        public string NoAnggota { get; set; }
        public string KLP { get; set; }
        public string Nama { get; set; }
        public string Gender { get; set; }
        public DateTime? TglLahir { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string NoHP { get; set; }
        public string NoIdentitas { get; set; }
        public string Jabatan { get; set; }
        public string Departemen { get; set; }
        public bool VIP { get; set; }
        public string KeteranganVIP { get; set; }
        public decimal? TambahIuran { get; set; }
        public bool Keluar { get; set; }
        public DateTime? TglKeluar { get; set; }
        public bool Active { get; set; }
    }
}