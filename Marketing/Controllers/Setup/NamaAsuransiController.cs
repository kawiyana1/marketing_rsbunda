﻿using Marketing.Entities.SIM;
using Marketing.Helper;
using Marketing.Models.Setup;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Marketing.Controllers.Setup
{
    [Authorize(Roles = "Marketing")]
    public class NamaAsuransiController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListNamaAsuransi.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Asuransi_ID.Contains(x.Value) ||
                                y.Asuransi_Name.Contains(x.Value) ||
                                y.Phone.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Asuransi_ID,
                        Nama = m.Asuransi_Name,
                        Phone = m.Phone
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, NamaAsuransiModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            id = s.MR_InsertNamaAsuransi(
                                model.Nama,
                                model.Alamat,
                                model.Phone,
                                model.Fax,
                                model.Website,
                                model.KontakPerson,
                                model.TypeAsuransi
                            ).FirstOrDefault();
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.MR_UpdateNamaAsuransi(
                                model.Id,
                                model.Nama,
                                model.Alamat,
                                model.Phone,
                                model.Fax,
                                model.Website,
                                model.KontakPerson,
                                model.TypeAsuransi
                            );
                            s.SaveChanges();
                            id = model.Id;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupNamaAsuransi-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_ListNamaAsuransi.FirstOrDefault(x => x.Asuransi_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.Asuransi_ID,
                            Nama = m.Asuransi_Name,
                            Alamat = m.Alamat,
                            Phone = m.Phone,
                            Fax = m.Fax,
                            Website = m.Website,
                            KontakPerson = m.KontakPerson,
                            TypeAsuransi = m.TypeAsuransiID
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}