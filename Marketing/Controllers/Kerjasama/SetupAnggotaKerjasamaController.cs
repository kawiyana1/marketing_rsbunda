﻿using Marketing.Entities.SIM;
using Marketing.Helper;
using Marketing.Models.Kerjasama;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Marketing.Controllers.Kerjasama
{
    [Authorize(Roles = "Marketing")]
    public class SetupAnggotaKerjasamaController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListAnggotaKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoIdentitas.Contains(x.Value) ||
                                y.Nama.Contains(x.Value) ||
                                y.NamaDepartemen.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value) ||
                                y.Perusahaan_Kode_Customer.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        IdPerusahaan = m.Perusahaan_Kode_Customer,
                        NamaPerusahaan = m.Nama_Customer,
                        NoAnggota = m.NoAnggota,
                        Nama = m.Nama,
                        Alamat = m.Alamat
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, SetupAnggotaKerjasamaModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = model.IdPerusahaan;
                        if (_process == "CREATE")
                        {
                            if (model.IdPerusahaan == 0) throw new Exception("Perusahaan harus diisi");
                            s.MR_InsertAnggotaKerjasama(
                                model.IdPerusahaan,
                                model.NoAnggota,
                                model.KLP,
                                model.Nama,
                                model.Gender,
                                model.TglLahir,
                                model.Alamat,
                                model.Phone,
                                model.NoHP,
                                model.NoIdentitas,
                                model.Jabatan,
                                model.Departemen,
                                model.VIP,
                                model.TambahIuran,
                                model.Keluar,
                                model.TglKeluar,
                                model.Active
                            );
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.MR_UpdateAnggotaKerjasama(
                                model.IdPerusahaan,
                                model.NoAnggota,
                                model.KLP,
                                model.Nama,
                                model.Gender,
                                model.TglLahir,
                                model.Alamat,
                                model.Phone,
                                model.NoHP,
                                model.NoIdentitas,
                                model.Jabatan,
                                model.Departemen,
                                model.VIP,
                                model.TambahIuran,
                                model.Keluar,
                                model.TglKeluar
                            );
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupSetupAnggotaKerjasama-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetAnggotaKerjasama.FirstOrDefault(x => x.NoAnggota == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            IdPerusahaan = m.CustomerKerjasamaID,
                            KodePerusahaan = m.Kode_Customer,
                            NamaPerusahaan = m.Nama_Customer,
                            NoAnggota = m.NoAnggota,
                            KLP = m.Klp,
                            Nama = m.Nama,
                            Gender = m.Gender,
                            TglLahir = m.TglLahir?.ToString("yyyy-MM-dd"),
                            Alamat = m.Alamat,
                            Phone = m.Phone,
                            NoHP = m.MobilePhone,
                            NoIdentitas = m.NoIdentitas,
                            Jabatan = m.JabatanKerjasama,
                            Departemen = m.NamaDepartemen,
                            VIP = m.VIP,
                            //KeteranganVIP = m.VIP
                            TambahIuran = m.IuranTambahan,
                            Keluar = m.Keluar,
                            TglKeluar = m.TglKeluar?.ToString("yyyy-MM-dd")
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K - U P

        [HttpPost]
        public string List_Perusahaan(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListKontrakKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Customer_Kode_Customer.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.CustomerKerjasamaID,
                        Kode = m.Customer_Kode_Customer,
                        Nama = m.Nama_Customer
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}