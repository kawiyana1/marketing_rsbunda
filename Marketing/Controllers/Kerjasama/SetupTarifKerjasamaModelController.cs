﻿using Marketing.Entities.SIM;
using Marketing.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Marketing.Models.Kerjasama;

namespace Marketing.Controllers.Kerjasama
{
    [Authorize(Roles = "Marketing")]
    public class SetupTarifKerjasamaModelController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListTarifKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.Customer_Kode_Customer.Contains(x.Value) ||
                            y.Nama_Customer.Contains(x.Value) ||
                            y.JenisKerjasama_JenisKerjasama.Contains(x.Value) ||
                            y.KelebihanPlafon.Contains(x.Value) ||
                            y.Kelas_NamaKelas.Contains(x.Value)
                           );

                        //if (x.Key == "Kategori" && !string.IsNullOrEmpty(x.Value))
                        //{
                        //    proses = proses.Where(y => y.KatVendor_Kategori_Name.Contains(x.Value)
                        //        );
                        //}
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        CustomerKerjasamaID = x.CustomerKerjasamaID,
                        Customer_Kode_Customer = x.Customer_Kode_Customer,
                        Nama_Customer = x.Nama_Customer,
                        JenisKerjasama_JenisKerjasama = x.JenisKerjasama_JenisKerjasama,
                        Kelas_NamaKelas = x.Kelas_NamaKelas,
                        StartDate = x.StartDate.ToString("dd/MM/yyyy"),
                        EndDate = x.EndDate.ToString("dd/MM/yyyy"),
                        CoPay = x.CoPay,
                        MaxHariRawatPerOpname = x.MaxHariRawatPerOpname,
                        MaxHariRawatPerTahun = x.MaxHariRawatPerTahun,
                        KelebihanPlafon = x.KelebihanPlafon,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, SetupTarifKerjasamaModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var id = 0;
                        var userid = User.Identity.GetUserId();
                        if (_process == "EDIT")
                        {
                            id = model.CUstomerKerjasamaID;
                            if (model.DetailJasa == null) model.DetailJasa = new List<SetupJasaTarifKerjasamaDetailModel>();
                            if (model.DetailPlafon == null) model.DetailPlafon = new List<SetupPlafonDetailModel>();
                            if (model.DetailFormularium == null) model.DetailFormularium = new List<SetupFormulariumDetailModel>();
                            s.MR_UpdateTarifKerjasama(
                                model.CUstomerKerjasamaID,
                                model.StartDate,
                                model.EndDate,
                                model.Copay,
                                model.MaxHariRawatPerOpname,
                                model.MaxHariRawatPerTahun,
                                model.KelebihanPlafon
                            );

                            var dJasa = s.MR_GetJasaTarifKerjasama.Where(x => x.CustomerKerjasamaID == model.CUstomerKerjasamaID).ToList();
                            var hargaid = s.MR_GetJasaKontrakMCU.FirstOrDefault();
                            var dPlafon = s.MR_GetDetailPlafon.Where(x => x.CustomerKerjasamaID == model.CUstomerKerjasamaID).ToList();
                            var dFormularium = s.MR_GetFormularium.Where(x => x.CustomerKerjasamaID == model.CUstomerKerjasamaID).ToList();
                            var jkerjasama = s.MR_GetFormularium.Where(x => x.CustomerKerjasamaID == model.CUstomerKerjasamaID).ToList();
                            foreach (var x in dJasa)
                            {
                                var _d = model.DetailJasa.FirstOrDefault(y => y.CUstomerKerjasamaID == x.CustomerKerjasamaID);

                            }
                            foreach (var x in dPlafon)
                            {
                                var _ddd = model.DetailPlafon.FirstOrDefault(y => y.CustomerKerjasamaID == x.CustomerKerjasamaID && y.KategoriPlafon == x.KategoriPlafon);
                             
                                if (_ddd == null) s.MR_DeleteDetailPlafon(x.CustomerKerjasamaID, x.KategoriPlafon);
                         
                            }
                            foreach (var x in dFormularium)
                            {
                                var _dd = model.DetailFormularium.FirstOrDefault(y => y.CustomerKerjasamaID == x.CustomerKerjasamaID && y.BarangID == x.Barang_ID);

                                if (_dd == null) s.MR_DeleteFormulariumKontrakKerjasama(x.CustomerKerjasamaID, x.Barang_ID);

                            }
                            foreach (var x in model.DetailJasa)
                            {


                                var _d = dJasa.FirstOrDefault(y => y.CustomerKerjasamaID == id);
                                if (_d == null)
                                {
                                    // new
                                    s.MR_InsertJasaTarifKerjasama(x.CUstomerKerjasamaID, x.ListHargaID, x.HargaLama, x.HargaBaru, x.Included, x.TglPerubahanHarga, x.HonorDefault, x.Honor);
                                }
                                else
                                {
                                    // edit
                                    s.MR_UpdateJasaTarifKerjasama(x.CUstomerKerjasamaID, x.ListHargaID, x.ListHargaIDLama, x.HargaLama, x.HargaBaru, x.Included, x.TglPerubahanHarga, x.HonorDefault, x.Honor);
                                }

                            }
                            foreach (var x in model.DetailPlafon)
                            {
                                var _d = dPlafon.FirstOrDefault(y => y.CustomerKerjasamaID == x.CustomerKerjasamaID);
                                if (_d == null)
                                {
                                    // new
                                    s.MR_InsertDetailPlafon(x.CustomerKerjasamaID, x.KategoriPlafon, x.Ditanggung, x.NilaiPlafon);
                                }
                                else
                                {
                                    // edit
                                    s.MR_UpdateDetailPlafon(id, x.KategoriPlafon, x.Ditanggung, x.NilaiPlafon);
                                }

                            }
                            foreach (var x in model.DetailFormularium)
                            {
                                var _d = dFormularium.FirstOrDefault(y => y.CustomerKerjasamaID == id);
                                var idkerjasamajenis = s.MR_ListTarifKerjasama.FirstOrDefault(xx => xx.CustomerKerjasamaID == id);
                                if (_d == null)
                                {
                                    // new
                                    s.MR_InsertFormulariumKontrakKerjasama(id, x.BarangID, x.JenisKerjasamaID, x.Include, x.Ditanggung);
                                }
                                else
                                {
                                    // edit
                                    s.MR_UpdateFormulariumKontrakKerjasama(id, x.BarangID, x.JenisKerjasamaID, x.Include, x.Ditanggung);
                                }

                            }
                            s.SaveChanges();
                        }
                        //else if (_process == "DELETE")
                        //{
                        //    id = model.Supplier_ID;
                        //    s.ADM_DeleteVendor(id.ToString());
                        //    s.SaveChanges();
                        //}

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"TarifKerjasama-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(short id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_ListTarifKerjasama.FirstOrDefault(x => x.CustomerKerjasamaID == id);

                    var hargaid = s.MR_GetJasaKontrakMCU.FirstOrDefault();
                    var dJasa = s.MR_GetJasaTarifKerjasama.Where(x => x.CustomerKerjasamaID == m.CustomerKerjasamaID).OrderBy(x => x.JasaID).ToList();
                    var dPlafon = s.MR_GetDetailPlafon.Where(y => y.CustomerKerjasamaID == m.CustomerKerjasamaID).OrderBy(y => y.CustomerKerjasamaID).ToList();
                    var dFormularium = s.MR_GetFormularium.Where(z => z.CustomerKerjasamaID == m.CustomerKerjasamaID).OrderBy(z => z.Kode_Barang).ToList();
                    var dd = s.MR_GetDetailKomponenKontrakMCU.Where(yy => yy.CustomerID == m.CustomerKerjasamaID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            CustomerKerjasamaID = m.CustomerKerjasamaID,
                            Customer_Kode_Customer = m.Customer_Kode_Customer,
                            Nama_Customer = m.Nama_Customer,
                            JenisKerjasama_JenisKerjasama = m.JenisKerjasama_JenisKerjasama,
                            Copay = m.CoPay,
                            MaxHariRawatPerOpname = m.MaxHariRawatPerOpname,
                            MaxHariRawatPerTahun = m.MaxHariRawatPerTahun,
                            KelebihanPlafon = m.KelebihanPlafon,
                            EndDate = m.EndDate.ToString("yyyy-MM-dd"),
                            StartDate = m.StartDate.ToString("yyyy-MM-dd"),
                        },
                        DetailJasa = dJasa.ConvertAll(x => new
                        {
                            CustomerKerjasamaID = m.CustomerKerjasamaID,
                            JasaID = x.JasaID,
                            ListHargaID = x.ListHargaID,
                            JasaName = x.JasaName,
                            NamaDokter = x.NamaDokter,
                            Harga_Lama = x.Harga_Lama,
                            Harga_Baru = x.Harga_Baru,
                            Ditanggung = x.Ditanggung,
                            Paket = x.Paket,
                            TglPerubahanHarga = x.TglPerubahanHarga.Value.ToString("yyyy-MM-dd"),
                            Lokasi = x.Lokasi,
                            Kelas = x.NamaKelas,
                            JenisKerjasama = x.JenisKerjasama,
                            Cyto = x.Cyto,
                            KategoriName = x.KategoriName,
                        }),
                        DetailPlafon = dPlafon.ConvertAll(y => new
                        {
                            CustomerKerjasamaID = m.CustomerKerjasamaID,
                            KategoriPlafon = y.KategoriPlafon,
                            Ditanggung = y.Ditanggung,
                            NilaiPlafon = y.NilaiPlafon,
                        }),
                        DetailFormularium = dFormularium.ConvertAll(z => new
                        {
                            CustomerKerjasamaID = m.CustomerKerjasamaID,
                            BarangID = z.Barang_ID,
                            Kode_Barang = z.Kode_Barang,
                            Nama_Barang = z.Nama_Barang,
                            Satuan_Stok = z.Satuan_Stok,
                            SubKategori = z.SubKategori,
                            Formularium = z.Formularium,
                            Ditanggung = z.Ditanggung,
                        }),
                        DetailDetail = dd.ConvertAll(x => new
                        {
                            JasaId = x.IDJasaInclude,
                            Id = x.IDKomponenID,
                            Nama = x.KomponenName,
                            Harga = x.Harga
                        })
                    }); ;
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion



        #region ===== GET JASA PAKET

        [HttpPost]
        public string GetJasaPaket(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var mm = s.MR_ListDetailPaketMCU.FirstOrDefault(x => x.MCUID == id);
                    var dpaketMCU = s.ADM_GetPaketMCU.Where(x => x.MCUID == id).ToList();
                    var dpaketdetailMCU = s.MR_ListDetailPaketMCU.Where(y => y.MCUID == id).ToList();
                    var dpaketkomponenMCU = s.MR_ListKomponenPaketMCU.Where(z => z.MCUID == id && z.JasaID == mm.JasaID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        detailjasa = dpaketdetailMCU.ConvertAll(y => new
                        {
                            MCUID = y.MCUID,
                            JasaID = y.JasaID,
                            JasaName = y.JasaName,
                            Harga = y.Harga,
                            SectionName = y.SectionName,
                            SectionID = y.SectionID,

                        }),
                        DetailKomponen = dpaketkomponenMCU.ConvertAll(z => new
                        {
                            Id = z.MCUID,
                            JasaId = z.JasaID,
                            Nama = z.KomponenName,
                            Harga = z.Harga
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string GetJasaPaketMCUKomponen(decimal id)
        {
            try
            {
                using (var s = new SIMEntities())
                {

                    var dpaketkomponenMCU = s.ADM_GetKomponenBiaya.Where(z => z.ListHargaID == id).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        DetailKomponen = dpaketkomponenMCU.ConvertAll(z => new
                        {
                            Id = z.ListHargaID,
                            KomponenName = z.KomponenName,
                            NilaiPersen = z.NilaiPersen,
                            HargaLama = z.HargaLama,
                            HargaBaru = z.HargaBaru
                        }),
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion

        #region ===== L O O K U P - Formularium

        [HttpPost]
        public string ListLookupFormularium(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetFormulariumTarifKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Kode_Satuan_Beli.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        BarangID = x.Barang_ID,
                        Kode_Barang = x.Kode_Barang,
                        Nama_Barang = x.Nama_Barang,
                        Kode_Satuan_Beli = x.Kode_Satuan_Beli,
                        SubKategori = x.SubKategori,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - Jasa

        [HttpPost]
        public string ListLookupJasa(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {

                    //var kelasid = filter.FirstOrDefault(x => x.Key == "Kelas").Value;

                    var kelas = s.SIMmKelas.FirstOrDefault();
                    var proses = s.MR_GetJasaKontrakMCU.Where(x => x.KelasID == "xx" || x.KelasID == kelas.KelasID /*&& x.KelasID == kelasid*/).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.JasaID.Contains(x.Value) ||
                                y.JasaName.Contains(x.Value));
                    }

                    totalcount = proses.Count();
                    if (totalcount == 0) throw new Exception("data tidak ada");
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        JasaID = x.JasaID,
                        JasaName = x.JasaName,
                        HargaKhusus = 1,
                        ListHargaID = x.ListHargaID,
                        DokterID = x.DokterID,
                        HargaLama = x.Harga_Lama,
                        HargaBaru = x.Harga_baru,
                        Ditanggung = 1,
                        Paket = 1,
                        TglHargaBaru = x.TglHargaBaru.ToString("yyyy/MM/dd"),
                        KelasID = x.KelasID,
                        Lokasi = x.Lokasi,
                        JenisKerjasama = x.JenisKerjasama,
                        Cyto = 1,
                        KategoriName = x.KategoriName,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion


    }
}
